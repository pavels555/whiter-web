import { ls, config } from 'npm-remote-ls'
import JSZip from 'jszip'
import { default as localforage } from "localforage";

function lsPromise(name, version, flatten) {
    return new Promise((resolve) => {
        version = version == '' ? 'latest' : version;
        ls(name, version, flatten, result => {
            resolve(result)
        })
    })
}

let makeTgzUrl = (packageVersion) => {

    let splits = packageVersion.split(/\/|@/).reverse();
    let version = splits[0];
    let name = splits[1];
    let scope = splits[2];
    if (scope) {
        return `https://registry.npmjs.cf/@${scope}/${name}/-/${name}-${version}.tgz`
    } else {
        return `https://registry.npmjs.cf/${name}/-/${name}-${version}.tgz`
    }
}

export async function getAllDependecies(name, devs) {
    config({
        registry: "https://registry.npmjs.cf",
        development: devs
    })

    let version = name.lastIndexOf('@') > 0 ? name.split('@').pop() : ''; 

    return await lsPromise(name, version, true)
        .then(namesArray => namesArray.map(name => {
            return { name, url: makeTgzUrl(name) }
        }))
}

export async function downloadFiles(packages, progressCb) {
    let files = packages.map(entry => {
        return {
            name: entry,
            url: makeTgzUrl(entry)
        }
    });

    let blobPromises = files.map(file =>
        fetch(file.url)
            .then(result => result.blob())
            .catch(err => console.log(err)));

    let blobs = await Promise.all(blobPromises);

    return files.map((file, index) => {
        return { ...file, blob: blobs[index] }
    })
}

async function fetchWithProgress(file, progress) {
    return fetch(file.url)
        .then(response => {
            if (!response.ok) {
                throw Error(response.status + ' ' + response.statusText)
            }

            let loaded = 0;

            return new Response(
                new ReadableStream({
                    start(controller) {
                        const reader = response.body.getReader();

                        read();
                        function read() {
                            reader.read().then(({ done, value }) => {
                                if (done) {
                                    controller.close();
                                    return;
                                }
                                loaded += value.byteLength;
                                file.
                                    progress({ loaded, total })
                                controller.enqueue(value);
                                read();
                            }).catch(error => {
                                console.error(error);
                                controller.error(error)
                            })
                        }
                    }
                })
            );
        })
}

export async function createZip(files) {
    let zip = new JSZip();

    let blobs = Promise.all(
        files.map(file => {
            return localforage.getItem(file.name)
                .then(value => {
                    let name = file.name;
                    let index = name.lastIndexOf('@');
                    name = name.substr(0, index) + '-' + name.substr(index + 1);
                    zip.file(name + ".tgz", value)
                })
        }));

    return await blobs
        .then(function () {
            return zip.generateAsync({ type: 'blob' })
        });
}