export function consoleWrap(cb, config) {
    let _log = console.log;
    let _error = console.error;
    let _warn = console.warn;

    let commonConfig = {
        duration: 5000,
        position: 'is-bottom',
        type: 'is-danger',
        queue: false
    }

    let logConfig = {
        ...commonConfig
    }

    let errorConfig = {
        ...commonConfig
    }

    let warnConfig = {
        ...commonConfig
    }

    console.log = function(msg) {
        cb(msg, logConfig);
        _log.apply(console, arguments);
    }

    console.error = function(msg) {
        //cb(msg, errorConfig);
        _error.apply(console, arguments);
    }

    console.warn = function(msg) {
        //cb(msg, warnConfig);
        _warn.apply(console, arguments);
    }
}